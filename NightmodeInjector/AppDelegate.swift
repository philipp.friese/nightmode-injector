//
//  AppDelegate.swift
//  NightmodeInjector
//
//  Created by Philipp Friese on 27.10.19.
//  Copyright © 2019 Philipp Friese. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }
    
    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }
    
}
