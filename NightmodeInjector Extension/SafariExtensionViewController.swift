//
//  SafariExtensionViewController.swift
//  NightmodeInjector Extension
//
//  Created by Philipp Friese on 27.10.19.
//  Copyright © 2019 Philipp Friese. All rights reserved.
//

import SafariServices

class SafariExtensionViewController: SFSafariExtensionViewController {
    
    static let shared: SafariExtensionViewController = {
        let shared = SafariExtensionViewController()
        shared.preferredContentSize = NSSize(width:320, height:240)
        return shared
    }()

}
