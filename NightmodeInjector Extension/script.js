document.addEventListener("DOMContentLoaded", function(event) {
  
  var head = document.head;
  var link = document.createElement("link");
  
  link.type = "text/css";
  link.rel = "stylesheet";
                          
  if (document.location.href.includes("news.ycombinator.com")) {
      link.href = "https://localhost/~philipp/stylesheets/hackernews_darkmode.css";
      console.log("Injecting Hackernews");
  } else if(document.location.href.includes("web.telegram.org")) {
      link.href = "https://localhost/~philipp/stylesheets/telegram_darkmode.css";
      console.log("Injecting Telegram");
  } else if(document.location.href.includes("old.reddit.com")) {
      link.href = "https://localhost/~philipp/stylesheets/reddit_darkmode.css";
      console.log("Injecting Reddit");
  }
  head.appendChild(link);
});
